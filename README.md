# Dynamo Table Migration Utility
---
## Configuration:
Configuring the utility is done by a Config object defined within a json file where the path is then passed to the utility as a param.

A single config file is required for each table that includes an export path to either place or load a table export. Table exports are all dumped within a single json file where the items. During import the items are parsed based on the table_type and inserted into the database as raw objects so that AWS can manage the typing.

- ### table_name : string - REQUIRED
    The name of the table that you want to import or export
- ### export_path : string - REQUIRED
    The path to export / import the table data.
- ### max_retry : int
    The maximum number of tries before raising an exception for API requests. Default is 3.
- ### retry_interval : int
    The maximum number of tries before raising an exception for API requests. Default is 3.
- ### table_type : enum - REQUIRED
    The type of table that you are import or exporting will dictate how the data in that table is imported.
- ### update_table : bool
    Wether or not you want to override existing table data (default is false to prevent accidental overrides).

Example config:
```
{
    "table_name": "con-aws-ue1-test-dev-ddb-ivr-messages",
    "export_path": "tables",
    "max_retry": 4,
    "retry_interval": 3,
    "table_type": "prompts",
    "update_table": true
}
```
## How To:

### Export:
To use the export feature via the python cli, simply invoke the following command (replace the file path with your config definition path) :
```
    py main.py export path/to/config/config.json
```
If you have exported successfully you will find a .json file in the export path that describes the table you have exported, and is ready to be imported into another environment or simply renamed to be cloned.
### Expected export example for an IVR prompts table: 
```
    {
        "instance_name": "con-aws-ue1-test-dev-ddb-ivr-messages",
        "items": [
            {
                "Messages": {
                    "M": {
                        "qualityAssurance": {
                            "M": {
                                "en-US": {
                                    "M": {
                                        "Message": {
                                            "S": "For quality assurance purposes your call may be monitored or recorded. If this is a medical emergency please hang up and dial 911."
                                        },
                                        "Type": {
                                            "S": "text"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "MsgGroup": {
                    "S": "MS_MAIN_ENGLISH"
                }
            }
        ]
    }
```

### Import:
To use the import feature via the python cli, simply invoke the following command (replace the file path with your config definition path) :
```
    py main.py import path/to/config/config.json
```

If you have imported successfully you will see something along the following. If this is the first time you are creating the database, its possible that provisioning will take longer than expected and cause time outs. You can adjust the interval to be longer or just re-run the script once the table is provisioned to migrate the data.
```
    2022-04-13 13:50:52,455 Found credentials in shared credentials file: ~/.aws/credentials
    2022-04-13 13:50:53,096 Config loaded...
    2022-04-13 13:50:53,096 ACCOUNT_ID: 555555555555555
    2022-04-13 13:50:53,096 TABLE_NAME: con-aws-ue1-test-dev-appdev-ddb-ivr-messages
    2022-04-13 13:50:53,096 EXPORT_PATH: tables
    2022-04-13 13:50:53,112 Importing con-aws-ue1-tes-dev-appdev-ddb-ivr-messages table into AWS...
    2022-04-13 13:50:54,039 Table not found, waiting 3 before retry. (0/4)
    2022-04-13 13:50:57,130 Table not found, waiting 3 before retry. (1/4)
    2022-04-13 13:51:00,242 Table not found, waiting 3 before retry. (2/4)
    2022-04-13 13:51:03,351 Table not found, waiting 3 before retry. (3/4)
    2022-04-13 13:51:06,456 Table Import Complete!
```