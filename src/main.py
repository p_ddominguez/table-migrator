import argparse
import sys
import logging
from table_migrator import TableMigrator
from utils.config import Config

EXPORT = 'export'
IMPORT = 'import'

"""
GLOBAL LOGGING CONFIGURATION
"""
FORMAT = '%(asctime)s %(message)s'
LOGGER = logging.getLogger('TableMigrator')
logging.basicConfig(format=FORMAT, level=logging.INFO)


def run(command, config_path) -> None:
    """Migrator Entry Point"""
    try:
        config: Config
        config = Config.load_config(config_path)
        LOGGER.info(f'Config loaded...')
        LOGGER.info(f'ACCOUNT_ID: {config.account_id}')
        LOGGER.info(f'TABLE_NAME: {config.table_name}')
        migrator = TableMigrator(config, LOGGER)
        migrator.import_table() if command == IMPORT else migrator.export_table()
    except Exception as e:
        LOGGER.error(f'Error during table migration: {e}')


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('command', help=f'{EXPORT}|{IMPORT} ')
    arg_parser.add_argument('config_path', help=f'Path to the configuration file', type=str)
    args = arg_parser.parse_args()

    if args.command != EXPORT and args.command != IMPORT:
        LOGGER.error(f'Valid commands include {EXPORT} or {IMPORT}.')
        LOGGER.error(f'eg ``py main.py <export> <config path>')
        sys.exit(1)

    run(args.command, args.config_path)
