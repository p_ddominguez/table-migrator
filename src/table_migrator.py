import json
import logging
from utils.config import Config
from models.export import TableExport
from table_manager import TableManager

class TableMigrator:
    """This Utility grabs a specificed dynamo db table if it exists and exports the table and its conents
    into a local export domain object. It also has the ability to import directly from a defined export domain
    objected into an AWS Environment to automate and standardize the process for table creation and item injection."""
    def __init__(self, config: Config, logger: logging.Logger) -> None:
        self.LOGGER = logger
        self.config = config
        self.table_manager = TableManager(config.ddb_client, config.ddb_resource, logger)

    def load_export(self, export_path, table_name) -> TableExport:
        """
        Loads a json file as an export object
        """
        try:
            compiled_path = f'{export_path}/{table_name}.json'
            with open(compiled_path, 'r') as export_file:
                table = TableExport.from_dict(json.load(export_file), table_name)
            return table
        except Exception as e:
            print(f'Error loading config: {e}')

    def import_table(self) -> bool:
        """Import the exported table from a local directory into the configured AWS env."""
        table_name = self.config.table_name

        self.LOGGER.info(f'Importing {table_name} table into AWS...')
        table_export = self.load_export(self.config.export_path, table_name)

        # TODO: Return waiter instead of syncro response.
        self.table_manager.try_create_table(
            table_export.instance_name,
            table_type=self.config.table_type,
            update_table=bool(self.config.update_table))

        # Insert or update the items.
        if len(table_export.items) > 0:
            table_type = self.config.table_type
            retry = self.config.max_retry
            interval = self.config.retry_interval
            [self.table_manager.try_create_item(table_name, item, table_type=table_type, retry=retry, interval=interval) for item in table_export.items]
        
        self.LOGGER.info(f'Table Import Complete!')
        return True

    def export_table(self) -> bool:
        """Export the requested table from a configured AWS env"""
        try:
            table_name = self.config.table_name
            self.LOGGER.info(f'Exporting {table_name} table from AWS...')
            
            raw_export = self.table_manager.try_read_table(table_name)
            export = TableExport(table_name)
            if raw_export.Count > 0:
                export.items = raw_export.Items

            export.write_to_disk(self.config.export_path)
            return True
        except Exception as e:
            self.LOGGER.error(f'Error exporting table from AWS: {e}')
            return False 