from constants.partition_keys import PartitionKeys
from constants.partition_keys import PartitionKeyTypes


class ItemInjector:
    """
    The ItemInjector contains handler delegates to be invoked based on the type of table that the item is being injected into.
    """

    @staticmethod
    def invoke(table=None, item=None, method_ptr=None) -> any:
        """
        Given a method pointer, a boto3 DDB client, and args, invokes the method passing the client and args as params.
        """
        if table is None:
            raise Exception(f'Missing table definition!')
        if method_ptr is None:
            raise Exception(f'Missing method pointer!')
        if item is None:
            raise Exception(f'Missing method args!')

        return method_ptr(table, item)

    @staticmethod
    def inject_holidays(table, item):
        """
        Injects a holiday item into the holiday table.
        """
        calendar_name = item[PartitionKeys.CALENDAR_NAME][PartitionKeyTypes.STRING]
        full_date = item[PartitionKeys.FULL_DATE][PartitionKeyTypes.STRING]
        holiday_name = item[PartitionKeys.HOLIDAY_NAME][PartitionKeyTypes.STRING]
        table.put_item(Item={PartitionKeys.CALENDAR_NAME: calendar_name, PartitionKeys.FULL_DATE: full_date,
                             PartitionKeys.HOLIDAY_NAME: holiday_name})
        return True

    @staticmethod
    def inject_config(table, args):
        """
        Injects a config item into the config table.
        """
        raise Exception(f'Unimplemented!')

    @staticmethod
    def inject_messages(table, _item) -> bool:
        """
        Injects prompts into the messages table.
        """
        messages = {}
        message_group = _item[PartitionKeys.MESSAGE_GROUPS][PartitionKeyTypes.STRING]
        for message_name in _item[PartitionKeys.MESSAGES][PartitionKeyTypes.MAP]:
            message_def = _item[PartitionKeys.MESSAGES][PartitionKeyTypes.MAP][message_name]
            locale = message_def[PartitionKeyTypes.MAP]
            locale_key = list(locale.keys())[0]
            message = locale[locale_key][PartitionKeyTypes.MAP][PartitionKeys.MESSAGE][PartitionKeyTypes.STRING]
            item = {
                locale_key: {
                    'Message': message,
                    'Type': "text"
                }
            }
            messages.update({message_name: item})
        table.put_item(Item={PartitionKeys.MESSAGE_GROUPS: message_group, PartitionKeys.MESSAGES: messages})
        return True
