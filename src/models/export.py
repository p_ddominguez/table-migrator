from datetime import datetime
import json
import os

class Export:
    """Abstract base class export"""
    def __init__(self, instance_name) -> None:
        self.instance_name = instance_name

    def __str__(self) -> str:
        """Dumps JSON repr of the export object replace nested DateTimes with custom mapping if found."""
        return json.dumps(self, indent=4, default=lambda o: o.__dict__)

    def write_to_disk(self, out_dir) -> bool:
        """Write the export object to the given directory as a json file."""
        base_path = f'{out_dir}/{self.instance_name}'
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        compiled_path = f'{base_path}.json'
        with open(compiled_path, 'wt') as out_file:
            out_file.write(self.__str__())


class TableExport(Export):
    """Extended Export object that represents a single Dynamo DB Table and its Items"""
    def __init__(self, table_name) -> None:
        super().__init__(table_name)
        self.items = []

    @staticmethod
    def from_dict(table_export_dict, table_name) -> any:
        """Attempts to map a dictionary object into a table export object.
        eg. loading exports from json will provide a raw dictionary object without class metho definitions."""
        try:
            table_export = TableExport(table_name)
            for k, v in table_export_dict.items():
                if k in table_export.__dict__.keys():
                    table_export.__dict__[k] = v
            return table_export
        except Exception as e:
            raise ValueError(f'Error when mapping dictionary to table export: {e}')