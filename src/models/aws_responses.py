import json

class AWSTableScanResponse:
    """AWS Model definition as defined by:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Client.scan"""
    def __init__(self) -> None:
        self.Items = None
        self.Count = None
        self.ScannedCount = None
        self.LastEvaluatedKey = None
        self.ConsumedCapacity = None

    @staticmethod
    def from_dict(response_dict) -> any:
        obj = AWSTableScanResponse()
        for k, v in response_dict.items():
            if k in obj.__dict__.keys():
                obj.__dict__[k] = v
        return obj

    def __str__(self) -> str:
        return json.dumps(self, default= lambda o: o.__dict__)

class AWSTableScanResponseDTO(AWSTableScanResponse):
    """Extended domain model that contains custom properties."""
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def from_dict(response_dict) -> any:
        obj = AWSTableScanResponseDTO()
        for k, v in response_dict.items():
            if k in obj.__dict__.keys():
                obj.__dict__[k] = v
        return obj