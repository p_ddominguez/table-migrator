import logging
import time
from models.aws_responses import AWSTableScanResponse, AWSTableScanResponseDTO
from botocore.exceptions import ClientError
from item_injector import ItemInjector
from table_builder import TableBuilder
from constants.table_types import TableTypes
from constants.partition_keys import PartitionKeys

class TableManager:
    """
    The TableManager class is intended to encapsulate method interactions with Dynamo DB Tables.
    """
    def __init__(self, client, ddb_resource, logger: logging.Logger) -> None:
        self.client = client
        self.ddb = ddb_resource
        self.LOGGER = logger
        self.item_injector_registry= {
            TableTypes.PROMPTS : ItemInjector.inject_messages,
            TableTypes.HOLIDAY : ItemInjector.inject_holidays,
            TableTypes.CONFIG : ItemInjector.inject_config }
        self.partition_key_registry= {
            TableTypes.PROMPTS : PartitionKeys.MESSAGE_GROUPS,
            TableTypes.HOLIDAY : PartitionKeys.CALENDAR_NAME,
            TableTypes.CONFIG : PartitionKeys.CONFIG }
        self.table_builder_registery= {
            TableTypes.PROMPTS : TableBuilder.create_prompts_table,
            TableTypes.HOLIDAY : TableBuilder.create_holiday_table,
            TableTypes.CONFIG : TableBuilder.create_config_table }

    def invoke(self, method_ptr, client, table_name, partition_key, update_table) -> any:
        """
        This method wraps a method pointer and invokes it with the forwarded args.
        """
        return method_ptr(client, table_name, partition_key, update_table)

    def try_read_table(self, table_name, retry_interval=2) -> AWSTableScanResponseDTO:
        """
        Attempts to scan all of the items in the dynamo table and append them to the export object for storage.
        """
        # TODO: Add retry impl
        aws_response: AWSTableScanResponse
        response = self.client.scan(TableName=table_name)

        # TODO: Param the await and retry interval via config
        self.LOGGER.info(f"Waiting {retry_interval} second(s) for AWS response...")
        time.sleep(retry_interval)

        # Mapping response dictionary to object
        aws_response = AWSTableScanResponse.from_dict(response)

        # TODO: Make this method recursive if 'LastEvalutedKey' is in the response.
        # self.LOGGER.info(f"Paginated: {'LastEvaluatedKey'in aws_response.__dict__.keys() and aws_response.LastEvaluatedKey is not None}")
        return AWSTableScanResponseDTO.from_dict(aws_response.__dict__)

    def try_create_table(self, table_name, table_type=None, update_table=False) -> None:
        """
        Try to create a table, gracefully handle table exist error and default to update.
        """
        if table_type is None:
            raise ValueError(f'Table Type must be defined!')
        
        #resolve the partition key based on the table_type
        partition_key = self.partition_key_registry[table_type]

        if partition_key is None:
            raise ValueError(f'Missing or invalid partition key defintion!')
        
        #resolve the method pointer based on the table_type
        method_pointer = self.table_builder_registery[table_type]

        if method_pointer is None:
            raise Exception(f'Missing or invalid method pointer!')

        #invoke the method 
        self.invoke(method_pointer, self.client, table_name, partition_key, update_table)

    def update_table(self, table_name) -> bool:
        """
        Update an existing table if it already exists. 
        """
        self.LOGGER.info(f'Updating Table: {table_name}')
        # TODO: Add implmentation
        return True

    def try_create_item(self, table_name, item, table_type=None, retry=3, interval=3, attempt=0) -> None:
        """
        Try to inject the item into the current table.
        """

        if table_type is None:
            raise ValueError(f'Table Type must be defined!')
        
        # Resolve the pointer based on table type
        method_pointer=self.item_injector_registry[table_type]

        try:
            ItemInjector.invoke(table=self.ddb.Table(table_name), method_ptr=method_pointer, item = item)
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'ResourceNotFoundException':
                if f"Requested resource not found" in error_msg:
                    if attempt >= retry:
                        raise Exception(f'Table could not be found: {table_name}')

                    self.LOGGER.info(f'Table not found, waiting {interval} second(s) before retry. Attempt ({attempt}/{retry})!')
                    time.sleep(interval)
                    attempt = attempt + 1
                    return self.try_create_item(table_name, item, table_type, retry, interval, attempt)
                raise Exception(f'Timed out waiting for AWS to Provision the Dynamo Table. Re-run the script with override enabled to inject items.')

