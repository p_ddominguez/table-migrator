import json
import boto3
from constants.table_types import TableTypes

class Config():
    """
    The configuration file contains all of the properties that are unique to the configuration of the 
    export or import.
    """
    def __init__(self) -> None:
        self.sts_client = boto3.client('sts')
        self.ddb_client = boto3.client('dynamodb')
        self.ddb_resource = boto3.resource('dynamodb')
        self.account_id = self.sts_client.get_caller_identity()["Account"]
        self.max_retry = 3
        self.retry_interval = 3
        self.update_table = False
        self.table_name = None
        self.export_path = None
        self.table_type = None


    @staticmethod
    def load_config(config_path) -> any:
        """
        Attempt to load a json file from the given path and returns a config object with the mapped values.
        TODO: Add support for env variable and self referencing.
        """
        config = Config()
        TABLE_TYPE_KEY = "table_type"
        with open(config_path, 'r') as config_file:
            config_json = json.load(config_file)
            for k, v in config_json.items():
                if k in config.__dict__.keys():
                    if str(k) == TABLE_TYPE_KEY:
                        if TableTypes.__dict__.get(str(v).upper()) is None:
                            raise ValueError(f'Invalid Table Type! {v}!')
                    config.__dict__[k] = v
        return config
