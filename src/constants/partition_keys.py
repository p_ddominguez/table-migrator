class PartitionKeys:
    MESSAGE_GROUPS = 'MsgGroup'
    MESSAGES = 'Messages'
    MESSAGE = 'Message'
    CONFIG = 'Config'
    CALENDAR_NAME = 'CalendarName'
    HOLIDAY_NAME = 'HolidayName'
    FULL_DATE = 'FullDate'


class PartitionKeyTypes:
    STRING = 'S'
    MAP = 'M'
    NUMBER = 'N'
