from botocore.exceptions import ClientError
from constants.key_types import KeyTypes
from constants.partition_keys import PartitionKeys


class TableBuilder:
    """
    The Table Builder encapsulates the methods for creating various flavors of commonly uses DDB schemas.
    """

    @staticmethod
    def create_holiday_table(client, table_name, partition_key, update_table):
        """
        This method creates the ddb holiday table as per the CHC Development standard.
        """
        try:
            client.create_table(
                TableName=table_name,
                KeySchema=[
                    {
                        'AttributeName': partition_key,
                        'KeyType': KeyTypes.HASH
                    },
                    {
                        'AttributeName': PartitionKeys.FULL_DATE,
                        'KeyType': KeyTypes.RANGE
                    }
                ],
                AttributeDefinitions=[
                    {
                        'AttributeName': partition_key,
                        'AttributeType': 'S'
                    },
                    {
                        'AttributeName': PartitionKeys.FULL_DATE,
                        'AttributeType': 'S'
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5
                }
            )
            return True
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'ResourceInUseException':
                if f"Table already exists" in error_msg:
                    if not update_table:
                        raise Exception(
                            f'This table already exists, to avoid accidental overrides the update feature is disabled by default!')

    @staticmethod
    def create_config_table(client, table_name, partition_key, update_table):
        raise Exception(f'Unimplemented!')

    @staticmethod
    def create_prompts_table(client, table_name, partition_key, update_table):
        """
        This method creates the ddb messages table as per the CHC Development standard.
        """
        try:
            client.create_table(
                TableName=table_name,
                KeySchema=[
                    {
                        'AttributeName': partition_key,
                        'KeyType': KeyTypes.HASH
                    }],
                AttributeDefinitions=[
                    {
                        'AttributeName': partition_key,
                        'AttributeType': 'S'
                    }],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5
                }
            )

            return True
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'ResourceInUseException':
                if f"Table already exists" in error_msg:
                    if not update_table:
                        raise Exception(
                            f'This table already exists, to avoid accidental overrides the update feature is disabled by default!')
